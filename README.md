# cqrs-event-source-resources

List of resources for learning CQRS, Event Sourcing, and related topics.


## Books & Web

- [ ] [CQRS Documents by Greg Young](https://cqrs.files.wordpress.com/2010/11/cqrs_documents.pdf)
- [ ] [CQRS Documents by Greg Young - ebook gen](https://github.com/keyvanakbary/cqrs-documents)
- [ ] [Designing Data Intensive Applications](https://dataintensive.net/) 
- [ ] [Microsoft CQRS Journey](https://docs.microsoft.com/en-us/previous-versions/msp-n-p/jj554200(v=pandp.10)) 
- [ ] [Microsoft Event Sourcing Doc](https://docs.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)
- [ ] [Microsoft CQRS Doc](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs)


## Video Links

- [ ] [Youtube Playlist - lots of links](https://youtube.com/playlist?list=PLMenfEYJUbyFHXnBBaLzwK-CPf-PnbsVz) 
- [ ] [Greg Young - A Decade of DDD, CQRS, Event Sourcing](https://youtu.be/LDW0QWie21s) 
- [ ] [Greg Young - Event Sourcing](https://youtu.be/8JKjvY4etTY) 
- [ ] [Greg Young - Stop Over Engineering](https://youtu.be/GRr4xeMn1uU) 
- [ ] [Scaling Event Sourcing For Netflix Downloads](https://youtu.be/rsSld8NycCU) 
- [ ] [Martin Kleppman - Event Sourcing and Stream Processing at Scale](https://youtu.be/avi-TZI9t2I)
- [ ] [Clean ASP.NET Core API using MediatR and CQRS](https://youtu.be/YzOBrVlthMk)


## Working Code Repositories

- [ ] [Oskar Dudycz - Example Code, Learning Guide, Resources](https://github.com/oskardudycz/EventSourcing.NetCore) 
- [ ] [Greg Young - Simple CQRS Example](https://github.com/gregoryyoung/m-r)
- [ ] [Awesome Domain Driven Design - list of resources](https://github.com/heynickc/awesome-ddd)

